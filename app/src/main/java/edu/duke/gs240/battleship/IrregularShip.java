package edu.duke.gs240.battleship;

import java.util.HashSet;

public class IrregularShip<T> extends BasicShip<T> {

    final String name;
    Placement p;
    static HashSet<Coordinate> makeCoords(String name, Placement where) {
        HashSet<Coordinate> s = new HashSet<Coordinate>();
        Coordinate upperLeft = where.getCoordinate();
        if (name == "Battleship") {
            if (where.getOrientation() == 'u') {
                s.add(
                  new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2));
                return s; 
            }
              
            if (where.getOrientation() == 'r') {
                s.add(
                  new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()));
                return s; 
            }
                
        
            if (where.getOrientation() == 'd') {
                s.add(
                  new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                return s; 
            }
                
            if (where.getOrientation() == 'l') { 
                s.add(
                  new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1));
                return s; 
            }  
            
        }

        if (name == "Carrier") {
            if (where.getOrientation() == 'u') {
                s.add(
                  new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()+1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn()+1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn()+1));
        
                return s; 
            }
              
            if (where.getOrientation() == 'r') {
                s.add(
                  new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 2));
                s.add(
                    new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 3));
                s.add(
                    new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 2));
                return s; 
            }
                
        
            if (where.getOrientation() == 'd') {
                s.add(
                  new Coordinate(upperLeft.getRow(), upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow() + 1, upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 2, upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 3, upperLeft.getColumn() + 1));
                s.add(
                    new Coordinate(upperLeft.getRow() + 4, upperLeft.getColumn() + 1));
                return s; 
            }
                
            if (where.getOrientation() == 'l') { 
                s.add(
                  new Coordinate(upperLeft.getRow(), upperLeft.getColumn()+2));
                s.add(
                    new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 3));
                s.add(
                    new Coordinate(upperLeft.getRow(), upperLeft.getColumn() + 4));
                s.add(
                    new Coordinate(upperLeft.getRow()+1, upperLeft.getColumn()));
                s.add(
                    new Coordinate(upperLeft.getRow()+1, upperLeft.getColumn()+1));
                s.add(
                    new Coordinate(upperLeft.getRow()+1, upperLeft.getColumn()+2));
                s.add(
                    new Coordinate(upperLeft.getRow()+1, upperLeft.getColumn()+3));
                return s; 
            }  
        }
        
        return s;
    }

    @Override
    public String getName() {
        return name;
    }

    public IrregularShip(String name, Placement where, T data, T onHit) {
        this(name, where, 
            new SimpleShipDisplayInfo<T>(data, onHit),
            new SimpleShipDisplayInfo<T>(null, data));
    }

    public IrregularShip(String name, Placement where, ShipDisplayInfo<T> info, ShipDisplayInfo<T> enemy_info) {
        // super() is implicit
        super(makeCoords(name, where), info, enemy_info, where);
        this.name = name;
    }
}

