package edu.duke.gs240.battleship;

import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T> {
    // private final Coordinate myLocation;
    protected HashMap<Coordinate, Boolean> myPieces;
    protected ShipDisplayInfo<T> myDisplayInfo;
    protected ShipDisplayInfo<T> enemyDisplayInfo;
    Placement p;
    // public BasicShip(Coordinate location) {
    //     myLocation = location;
    // }

    // public BasicShip(Coordinate c) {
    //     myPieces = new HashMap<Coordinate, Boolean>();
    //     myPieces.put(c, false);
    // }

    public BasicShip(Iterable<Coordinate> where, ShipDisplayInfo<T> self_info, ShipDisplayInfo<T> enemy_info, Placement pl) {
        myDisplayInfo = self_info;
        enemyDisplayInfo = enemy_info;
        myPieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate c: where) {
            myPieces.put(c, false);
        }
        p = pl;
    }

    // public BasicShip(BasicShip<T> ship) {
    //     myDisplayInfo = ship.myDisplayInfo;
    //     enemyDisplayInfo = ship.enemyDisplayInfo;
    //     myPieces = ship.myPieces;
    // }

    @Override
    public boolean occupiesCoordinates(Coordinate where) {
        // TODO Auto-generated method stub
        // return where.equals(myLocation);
        return myPieces.containsKey(where);
    }

    @Override
    public boolean isSunk() {
        // TODO Auto-generated method stub
        for (Boolean v: myPieces.values()) {
            if (v == false) return false;
        }
        return true;
    }

    @Override
    public void recordHitAt(Coordinate where) {
        // TODO Auto-generated method stub
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    @Override
    public boolean wasHitAt(Coordinate where) {
        // TODO Auto-generated method stub
        checkCoordinateInThisShip(where);
        return myPieces.get(where) == true;
    }

    @Override
    public T getDisplayInfoAt(Coordinate where, boolean forSelf) {
        //TODO this is not right.  We need to
        //look up the hit status of this coordinate
        checkCoordinateInThisShip(where);
        if (forSelf) {
            return myDisplayInfo.getInfo(where, myPieces.get(where));
        }
        
        return enemyDisplayInfo.getInfo(where, myPieces.get(where));
    }
    
    protected void checkCoordinateInThisShip(Coordinate c) {
        if (!myPieces.containsKey(c)) 
            throw new IllegalArgumentException("Coordinate is not part of the ship");
    }

    /**
     * Get all of the Coordinates that this Ship occupies.
     * @return An Iterable with the coordinates that this Ship occupies
     */
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

    public HashMap<Coordinate, Boolean> getMyPieces() {
        return myPieces;
    }

    public Placement getPlacement() {
        return p;
    }
}
