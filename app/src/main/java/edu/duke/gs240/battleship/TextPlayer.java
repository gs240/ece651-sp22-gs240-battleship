package edu.duke.gs240.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer {
    final String name;
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    int autoIndex = 0;
    ArrayList<Placement> autoList;
    V1ShipFactory factory;
    boolean isHuman;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    int sonarActionCount;
    int moveActionCount;

    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> factory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> factory.makeDestroyer(p));
        shipCreationFns.put("Carrier", (p) -> factory.makeCarrier(p));
        shipCreationFns.put("Battleship", (p) -> factory.makeBattleship(p));
    }

    public boolean checkLose() {
        return theBoard.checkAllSunk();
    }

    public String getName() {
        return name;
    }

    public Board<Character> getBoard() {
        return theBoard;
    }

    public BoardTextView getBoardView() {
        return view;
    }

    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        return new Placement(s);
    }

    public Coordinate readCoordinate(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        return new Coordinate(s);
    }

    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        if (!isHuman) {
            Placement p = autoList.get(autoIndex);
            autoIndex += 1;
            Ship<Character> s = createFn.apply(p);
            if (s == null) {
                out.println("Wrong option for orientation");
            }
            theBoard.tryAddShip(s);
            return;
        }
        
        while (true) {
            try {
                Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
                Ship<Character> s = createFn.apply(p);
                if (s == null) {
                    out.println("Wrong option for orientation");
                    continue;
                }
                String status = theBoard.tryAddShip(s);
                if(status != null) {
                    out.println(status); 
                    continue;
                }
                else
                    out.println(view.displayMyOwnBoard());
                break;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            }  catch (IOException e) {
                out.println(e.getMessage());
            }
        }
        
        return; 
    }

    public void doPlacementPhase() throws IOException {
        if (isHuman) {
            view.displayMyOwnBoard();
            String message = 
                "--------------------------------------------------------------------------------\n"+
                "Player: you are going to place the following ships (which are all\n"+
                "rectangular). For each ship, type the coordinate of the upper left\n"+
                "side of the ship, followed by either H (for horizontal) or V (for\n"+
                "vertical).  For example M4H would place a ship horizontally starting\n"+
                "at M4 and going to the right.  You have\n\n"+
                "2 \"Submarines\" ships that are 1x2\n"+
                "3 \"Destroyers\" that are 1x3\n"+
                "3 \"Battleships\" that are 1x4\n"+
                "2 \"Carriers\" that are 1x6\n"+
                "--------------------------------------------------------------------------------";
            out.println(message);
        }
       
        for (String shipName : shipsToPlace) {
            doOnePlacement(name, shipCreationFns.get(shipName));
        }  

        if (isHuman) view.displayMyOwnBoard();
    }

    public void doOneMove() {
        String message = 
            "--------------------------------------------------------------------------------\n"+
            "Please input the coordinate of the ship to launch a move.\n"+
            "--------------------------------------------------------------------------------";
        while (true) {
            try{
                out.println(message);
                Coordinate source = readCoordinate("Player " 
                    + name + " which ship do you want to move?\n");

                Ship<Character> ship = theBoard.getShipFromCoordinate(source);
                if (ship == null) {
                    out.println("No ship exists on the given target, retype it please.");
                    continue;
                }

                Placement target = readPlacement("Player " 
                    + name + " what is your target address to place the ship?\n");
                Ship<Character> newShip = shipCreationFns.get(ship.getName()).apply(target);
                if (newShip == null) {
                    out.println("Wrong placement for this ship");
                    continue; //wrong placement for this ship.
                }
                theBoard.deleteShip(ship);
                String status = theBoard.checkAddShip(newShip);
                if (status != null) {
                    theBoard.tryAddShip(ship);
                    out.println(status);
                    continue;
                }

                //Mark hitted on the newShip
                // newShip.transferHitStatus(ship);
                theBoard.transferHitStatus(newShip, ship);
                theBoard.tryAddShip(newShip);


                out.println("move successfully");
                break;
            } catch(Exception e) {
                out.println(e.getMessage());
            }
        }
    }

    public void doOneSonar() {
        
        String message = 
            "--------------------------------------------------------------------------------\n"+
            "Please input the coordinate to launch a sonar.\n"+
            "--------------------------------------------------------------------------------";
        ArrayList<String> nameList = new ArrayList<>();
        nameList.add("Submarine");
        nameList.add("Battleship");
        nameList.add("Carrier");
        nameList.add("Destroyer");
        while (true) {
            out.println(message);
            try{
                Coordinate co = readCoordinate("Player " + name + " where do you want to launch an sonar?\n");
                HashMap<String, Integer> m = theBoard.getSonar(co);

                for (String name : nameList) {
                    int count = m.containsKey(name) ?  m.get(name) : 0;
                    out.println(name + "s occupy " + count + " squares");
                }
                break;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            } catch (IOException e) {
                out.println(e.getMessage());
            }
        }
        return;

    }

    public void doOneAttack(Board<Character> enemy_board) throws IOException {
        String message = 
            "--------------------------------------------------------------------------------\n"+
            "Please input the coordinate to launch an attack.\n"+
            "--------------------------------------------------------------------------------";
        while (true) {
            out.println(message);
            try{
                Coordinate co = readCoordinate("Player " + name + " where do you want to launch an attack?\n");
                Ship<Character> found = enemy_board.fireAt(co);
                if (found == null) {
                    out.println("You missed!");
                } else {
                    out.println("You hit a " + found.getName());
                }
                break;
            } catch (IllegalArgumentException e) {
                out.println(e.getMessage());
            } catch (IOException e) {
                out.println(e.getMessage());
            }
        }
        return;
    }

    public void playOneTurn(Board<Character> enemy_board, BoardTextView enemy_view, TextPlayer other) {
        if (!isHuman) {
            out.println("Player " + name + " is a computer, no information is shown");
            enemy_board.fireAt(new Coordinate("A5"));
            return;
        }
        try {
            out.println(view.displayMyBoardWithEnemyNextToIt(enemy_view, "Your ocean", "Player "+other.name+" 's ocean"));
            out.println("This is Player " + name + "'s turn:");
            out.println(
                "Possible actions for Player " + name 
                + ":\n F Fire at a square\n M Move a ship to another square ("
                + moveActionCount + " remaining) \n S Sonar scan (" 
                + sonarActionCount + " remaining))"
            );
            
            String action = inputReader.readLine();
            out.println("action is " + action);
            if (action.equals("S") && sonarActionCount > 0) {
                doOneSonar();
                sonarActionCount -= 1;
            } else if (action.equals("M") && moveActionCount > 0){
                doOneMove();
                moveActionCount -= 1;
            } else {
                doOneAttack(enemy_board); 
            }

        } catch (Exception e) {
            out.println(e.getMessage());
            return;
        }
        return;
    }

    public TextPlayer(String n, Board<Character> b, BufferedReader reader, PrintStream o, V1ShipFactory f, boolean is_human) {
        name = n;
        theBoard = b;
        inputReader = reader;
        out = o;
        factory = f;
        isHuman = is_human;
        view = new BoardTextView(theBoard);
        shipsToPlace = new ArrayList<>();
        shipCreationFns = new HashMap<>();
        setupShipCreationList();
        setupShipCreationMap();
        sonarActionCount = 3;
        moveActionCount = 3;
        autoIndex = 0;
        autoList = new ArrayList<>();
        if (!isHuman) {
            autoList.add(new Placement("A0V"));
            autoList.add(new Placement("A1V"));
            autoList.add(new Placement("A2V"));
            autoList.add(new Placement("A3V"));
            autoList.add(new Placement("A4V"));
            autoList.add(new Placement("E3U"));
            autoList.add(new Placement("H3U"));
            autoList.add(new Placement("K3L"));
            autoList.add(new Placement("O3R"));
            autoList.add(new Placement("R3L"));

        }
    }

}
