package edu.duke.gs240.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
    final String name;
    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> s = new HashSet<Coordinate>();
        for (int h = 0; h < height; ++h) {
            for (int w = 0; w < width; ++w) {
                s.add(new Coordinate(upperLeft.getRow()+h, upperLeft.getColumn()+w));
            }
        }
        return s;
    }

    @Override
    public String getName() {
        return name;
    }

    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit, new Placement("A0V"));
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit, Placement p) {
        this(name, upperLeft, width, height, 
            new SimpleShipDisplayInfo<T>(data, onHit),
            new SimpleShipDisplayInfo<T>(null, data), p);
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
        this(name, upperLeft, width, height, 
            new SimpleShipDisplayInfo<T>(data, onHit),
            new SimpleShipDisplayInfo<T>(null, data), new Placement("A0V"));
    }
    public RectangleShip(String name, Coordinate upperLeft, T data, T onHit, Placement p) {
        this(name, upperLeft, 1, 1, data, onHit, p);
    }

    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> info, ShipDisplayInfo<T> enemy_info, Placement p) {
        // super() is implicit
        super(makeCoords(upperLeft, width, height), info, enemy_info, p);
        this.name = name;
    }
    
}
