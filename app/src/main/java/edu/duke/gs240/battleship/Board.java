package edu.duke.gs240.battleship;

import java.util.HashMap;

public interface Board<T> {
    public int getWidth();
    public T whatIsAtForSelf(Coordinate where);
    public T whatIsAtForEnemy(Coordinate where);
    public String tryAddShip(Ship<T> toAdd);
    public HashMap<String, Integer> getSonar(Coordinate co);
    public String checkAddShip(Ship<T> toAdd);
    public String deleteShip(Ship<T> toDelete);
    public Ship<T> getShipFromCoordinate(Coordinate c);
    public int getHeight();
    public Ship<T> fireAt(Coordinate c);
    public boolean checkAllSunk();
    public void transferHitStatus(Ship<T> newShip, Ship<T> oldShip);
}