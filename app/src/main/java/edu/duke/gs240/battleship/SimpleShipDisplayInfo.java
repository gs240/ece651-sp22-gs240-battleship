package edu.duke.gs240.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {

    T myData;
    T onHit;
    @Override
    public T getInfo(Coordinate where, boolean hit) {
        // TODO Auto-generated method stub
        if (hit) return onHit;
        return myData;
    }

    public SimpleShipDisplayInfo(T data, T hit) {
        myData = data;
        onHit = hit;
    }
    
}