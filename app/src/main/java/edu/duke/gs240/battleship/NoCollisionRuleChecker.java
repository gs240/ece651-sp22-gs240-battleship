package edu.duke.gs240.battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T>{
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {

        for (Coordinate co : theShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(co) != null) 
                return "That placement is invalid: the ship overlaps another ship.";
        }

        return null;
    }

    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
}
