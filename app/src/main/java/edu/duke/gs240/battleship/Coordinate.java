package edu.duke.gs240.battleship;

public class Coordinate {
    private int row;
    private int column;

    public Coordinate(int r, int c) {
        if (r < 0 || r > 25 || c < 0 || c > 9) {
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
        row = r;
        column = c;
    }

    public Coordinate(String descr) {
        if (descr.length() != 2) {
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
        if (descr.charAt(0) >= 'a' && descr.charAt(0) <= 'z') 
            row = descr.charAt(0) - 'a';
        else 
            row = descr.charAt(0) - 'A';

        column = descr.charAt(1) - '0';
        if (row < 0 || row > 25 || column < 0 || column > 9) {
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public String toString() {
        return "("+row+", " + column+")";
    }
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }
}
