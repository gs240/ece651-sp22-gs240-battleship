package edu.duke.gs240.battleship;

public class V1ShipFactory implements AbstractShipFactory<Character> {
    
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        return new RectangleShip<Character>(
            name, where.getCoordinate(), w, h, letter, '*', where);
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
      if (where.getOrientation() == 'v')
        return createShip(where, 1, 2, 's', "Submarine");
      if (where.getOrientation() == 'h')
        return createShip(where, 2, 1, 's', "Submarine");

      return null;
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
      if (where.getOrientation() == 'v')
        return createShip(where, 1, 3, 'd', "Destroyer");
      if (where.getOrientation() == 'h')
        return createShip(where, 3, 1, 'd', "Destroyer");

      return null;
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
      if (where.getOrientation() != 'u' &&
          where.getOrientation() != 'r' &&
          where.getOrientation() != 'd' &&
          where.getOrientation() != 'l')
          return null;

      return new IrregularShip<Character>("Battleship", where, 'b', '*');  
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
      if (where.getOrientation() != 'u' &&
          where.getOrientation() != 'r' &&
          where.getOrientation() != 'd' &&
          where.getOrientation() != 'l')
          return null;

      return new IrregularShip<Character>("Carrier", where, 'c', '*');
    }
}
