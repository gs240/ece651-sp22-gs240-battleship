package edu.duke.gs240.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class BattleShipBoard<T> implements Board<T> {
    private final int width;
    private final int height;
    final ArrayList<Ship<T>> myShips;
    HashSet<Coordinate> enemyMisses;
    HashMap<Coordinate, T> enemyHits;
    final T missInfo;
    private final PlacementRuleChecker<T> placementChecker;
    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }

    public boolean checkAllSunk() {
        for (Ship<T> ship : myShips) {
            if(ship.isSunk() == false) return false;
        }
        return true;
    }

    public Ship<T> fireAt(Coordinate c) {

        for (Ship<T> ship: myShips) {
            if(ship.occupiesCoordinates(c)) {
                ship.recordHitAt(c);
                enemyHits.put(c, whatIsAt(c, false));
                return ship;
            }
        }

        enemyMisses.add(c);
        return null;
    }

    public Ship<T> getShipFromCoordinate(Coordinate c) {
        for (Ship<T> ship: myShips) {
            if(ship.occupiesCoordinates(c)) {
                return ship;
            }
        }
        return null;
    }

    public PlacementRuleChecker<T> getChecker() {
        return placementChecker;
    }

    public String tryAddShip(Ship<T> toAdd) {
        String res = placementChecker.checkMyRule(toAdd, this);
        if (res != null) return res;
            
        myShips.add(toAdd);
        return null;
    }

    public String checkAddShip(Ship<T> toAdd) {
        String res = placementChecker.checkMyRule(toAdd, this);
        return res;
    }

    public Coordinate getCoordinateByIndex(String name, int index, Placement p) {
        int offset_x = 0;
        int offset_y = 0;

        if (name.equals("Carrier")) {
            if (p.getOrientation() == 'u') {
                if (index == 1) {
                    offset_x = 0;
                    offset_y = 0;   
                } else if (index == 2) {
                    offset_x = 1;
                    offset_y = 0;   
                } else if (index == 3) {
                    offset_x = 2;
                    offset_y = 0;   
                } else if (index == 4) {
                    offset_x = 3;
                    offset_y = 0;   
                } else if (index == 5) {
                    offset_x = 1;
                    offset_y = 2;   
                } else if (index == 6) {
                    offset_x = 1;
                    offset_y = 3;   
                } else if (index == 7) {
                    offset_x = 1;
                    offset_y = 4;   
                }
            } else if (p.getOrientation() == 'r') {
                if (index == 1) {
                    offset_x = 0;
                    offset_y = 4;   
                } else if (index == 2) {
                    offset_x = 0;
                    offset_y = 3;   
                } else if (index == 3) {
                    offset_x = 0;
                    offset_y = 2;   
                } else if (index == 4) {
                    offset_x = 0;
                    offset_y = 1;   
                } else if (index == 5) {
                    offset_x = 1;
                    offset_y = 2;   
                } else if (index == 6) {
                    offset_x = 1;
                    offset_y = 1;   
                } else if (index == 7) {
                    offset_x = 1;
                    offset_y = 0;   
                }
            } else if (p.getOrientation() == 'd') {
                if (index == 1) {
                    offset_x = 4;
                    offset_y = 1;   
                } else if (index == 2) {
                    offset_x = 3;
                    offset_y = 1;   
                } else if (index == 3) {
                    offset_x = 2;
                    offset_y = 1;   
                } else if (index == 4) {
                    offset_x = 1;
                    offset_y = 1;   
                } else if (index == 5) {
                    offset_x = 2;
                    offset_y = 0;   
                } else if (index == 6) {
                    offset_x = 1;
                    offset_y = 0;   
                } else if (index == 7) {
                    offset_x = 0;
                    offset_y = 0;   
                }
            } else if (p.getOrientation() == 'l') {
                if (index == 1) {
                    offset_x = 1;
                    offset_y = 0;   
                } else if (index == 2) {
                    offset_x = 1;
                    offset_y = 1;   
                } else if (index == 3) {
                    offset_x = 1;
                    offset_y = 2;   
                } else if (index == 4) {
                    offset_x = 1;
                    offset_y = 3;   
                } else if (index == 5) {
                    offset_x = 0;
                    offset_y = 2;   
                } else if (index == 6) {
                    offset_x = 0;
                    offset_y = 3;   
                } else if (index == 7) {
                    offset_x = 0;
                    offset_y = 4;   
                }
            } 
        } else if (name.equals("Battleship")) {
            if (p.getOrientation() == 'u') {
                if (index == 1) {
                    offset_x = 0;
                    offset_y = 1;   
                } else if (index == 2) {
                    offset_x = 1;
                    offset_y = 0;   
                } else if (index == 3) {
                    offset_x = 1;
                    offset_y = 1;   
                } else if (index == 4) {
                    offset_x = 1;
                    offset_y = 2;   
                }
            } else if (p.getOrientation() == 'r') {
                if (index == 1) {
                    offset_x = 1;
                    offset_y = 1;   
                } else if (index == 2) {
                    offset_x = 0;
                    offset_y = 0;   
                } else if (index == 3) {
                    offset_x = 1;
                    offset_y = 0;   
                } else if (index == 4) {
                    offset_x = 2;
                    offset_y = 0;   
                }
            } else if (p.getOrientation() == 'd') {
                if (index == 1) {
                    offset_x = 1;
                    offset_y = 1;   
                } else if (index == 2) {
                    offset_x = 0;
                    offset_y = 2;   
                } else if (index == 3) {
                    offset_x = 0;
                    offset_y = 1;   
                } else if (index == 4) {
                    offset_x = 0;
                    offset_y = 0;   
                }
            } else if (p.getOrientation() == 'l') {
                if (index == 1) {
                    offset_x = 1;
                    offset_y = 0;   
                } else if (index == 2) {
                    offset_x = 2;
                    offset_y = 1;   
                } else if (index == 3) {
                    offset_x = 1;
                    offset_y = 1;   
                } else if (index == 4) {
                    offset_x = 0;
                    offset_y = 1;   
                }
            }
        } else if (name.equals("Submarine")) {
            if (p.getOrientation() == 'v') {
                if (index == 1) {
                    offset_x = 0;
                    offset_y = 0;   
                } else if (index == 2) {
                    offset_x = 1;
                    offset_y = 0;   
                }
            } else if (p.getOrientation() == 'h') {
                if (index == 1) {
                    offset_x = 0;
                    offset_y = 0;   
                } else if (index == 2) {
                    offset_x = 0;
                    offset_y = 1;   
                }
            }
        } else if (name.equals("Destroyer")) {
            if (p.getOrientation() == 'v') {
                if (index == 1) {
                    offset_x = 0;
                    offset_y = 0;   
                } else if (index == 2) {
                    offset_x = 1;
                    offset_y = 0;   
                } else if (index == 3) {
                    offset_x = 2;
                    offset_y = 0;   
                }
            } else if (p.getOrientation() == 'h') {
                if (index == 1) {
                    offset_x = 0;
                    offset_y = 0;   
                } else if (index == 2) {
                    offset_x = 0;
                    offset_y = 1;   
                } else if (index == 3) {
                    offset_x = 0;
                    offset_y = 2;   
                } 
            } 
        }
        
        return new Coordinate(
            p.getCoordinate().getRow() + offset_x, 
            p.getCoordinate().getColumn() + offset_y
        );
    }

    public int getRelativeIndex(String name, Placement p, int offset_x, int offset_y) {
        if (name.equals("Submarine")) {
            if (p.getOrientation() == 'v') {
                if (offset_x == 0 && offset_y == 0) return 1;
                if (offset_x == 1 && offset_y == 0) return 2;
                return -1;
            } else if(p.getOrientation() == 'h') {
                if (offset_x == 0 && offset_y == 0) return 1;
                if (offset_x == 1 && offset_y == 0) return 2;
                return -1;
            }
        } else if (name.equals("Destroyer")) {
            if (p.getOrientation() == 'v') {
                if (offset_x == 0 && offset_y == 0) return 1;
                if (offset_x == 1 && offset_y == 0) return 2;
                if (offset_x == 2 && offset_y == 0) return 3;
                return -1;
            } else if(p.getOrientation() == 'h') {
                if (offset_x == 0 && offset_y == 0) return 1;
                if (offset_x == 0 && offset_y == 1) return 2;
                if (offset_x == 0 && offset_y == 2) return 3;
                return -1;
            }
        } else if (name.equals("Carrier")) {
            if (p.getOrientation() == 'u') {
                if (offset_x == 0 && offset_y == 0) return 1;
                if (offset_x == 1 && offset_y == 0) return 2;
                if (offset_x == 2 && offset_y == 0) return 3;
                if (offset_x == 3 && offset_y == 0) return 4;
                if (offset_x == 1 && offset_y == 2) return 5;
                if (offset_x == 1 && offset_y == 3) return 6;
                if (offset_x == 1 && offset_y == 4) return 7;
                return -1;
            } else if(p.getOrientation() == 'r') {
                if (offset_x == 0 && offset_y == 1) return 4;
                if (offset_x == 0 && offset_y == 2) return 3;
                if (offset_x == 0 && offset_y == 3) return 2;
                if (offset_x == 0 && offset_y == 4) return 1;
                if (offset_x == 1 && offset_y == 0) return 7;
                if (offset_x == 1 && offset_y == 1) return 6;
                if (offset_x == 1 && offset_y == 2) return 5;
                return -1;
            } else if(p.getOrientation() == 'd') {
                if (offset_x == 0 && offset_y == 0) return 7;
                if (offset_x == 1 && offset_y == 0) return 6;
                if (offset_x == 2 && offset_y == 0) return 5;
                if (offset_x == 1 && offset_y == 1) return 4;
                if (offset_x == 1 && offset_y == 2) return 3;
                if (offset_x == 1 && offset_y == 3) return 2;
                if (offset_x == 1 && offset_y == 4) return 1;
                return -1;
            } else if(p.getOrientation() == 'l') {
                if (offset_x == 0 && offset_y == 2) return 5;
                if (offset_x == 0 && offset_y == 3) return 6;
                if (offset_x == 0 && offset_y == 4) return 7;
                if (offset_x == 1 && offset_y == 0) return 1;
                if (offset_x == 1 && offset_y == 1) return 2;
                if (offset_x == 1 && offset_y == 2) return 3;
                if (offset_x == 1 && offset_y == 3) return 4;
                return -1;
            }
        } else if (name.equals("Battleship")) {
            if (p.getOrientation() == 'u') {
                if (offset_x == 0 && offset_y == 1) return 1;
                if (offset_x == 1 && offset_y == 0) return 2;
                if (offset_x == 1 && offset_y == 1) return 3;
                if (offset_x == 1 && offset_y == 2) return 4;
                return -1;
            } else if(p.getOrientation() == 'r') {
                if (offset_x == 0 && offset_y == 0) return 2;
                if (offset_x == 1 && offset_y == 0) return 3;
                if (offset_x == 2 && offset_y == 0) return 4;
                if (offset_x == 1 && offset_y == 1) return 1;
                return -1;
            } else if(p.getOrientation() == 'd') {
                if (offset_x == 0 && offset_y == 0) return 4;
                if (offset_x == 0 && offset_y == 1) return 3;
                if (offset_x == 0 && offset_y == 2) return 2;
                if (offset_x == 1 && offset_y == 1) return 1;
                return -1;
            } else if(p.getOrientation() == 'l') {
                if (offset_x == 0 && offset_y == 1) return 4;
                if (offset_x == 1 && offset_y == 1) return 3;
                if (offset_x == 2 && offset_y == 1) return 2;
                if (offset_x == 1 && offset_y == 0) return 1;
                return -1;
            }
        }
        return -1;
    }

    public void transferHitStatus(Ship<T> newShip, Ship<T> oldShip) {
        for (Coordinate c: oldShip.getMyPieces().keySet()) {
            if (oldShip.getMyPieces().get(c) == true) {
                int offset_x = c.getRow() - oldShip.getPlacement().getCoordinate().getRow();
                int offset_y = c.getColumn() - oldShip.getPlacement().getCoordinate().getColumn();
                int square_index = getRelativeIndex(oldShip.getName(), oldShip.getPlacement(), offset_x, offset_y);
                Coordinate hit_coordinate = getCoordinateByIndex(oldShip.getName(), square_index, newShip.getPlacement());
                newShip.getMyPieces().put(hit_coordinate, true);
            }
        }
        return;
    }

    public String deleteShip(Ship<T> toDelete) {
        myShips.remove(toDelete);
        return null;
    }

    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
      }

    protected T whatIsAt(Coordinate where, boolean isSelf) {
        if (isSelf == false) {
            // Considering the case of moving
            if (enemyMisses.contains(where)) 
                return missInfo;
            if (enemyHits.containsKey(where)) 
                return enemyHits.get(where);
        }

        for (Ship<T> s: myShips) {
            if (s.occupiesCoordinates(where)) {
              return s.getDisplayInfoAt(where, isSelf);
            }
        }

        return null;
    }

    public HashMap<String, Integer> getSonar(Coordinate co) {
        HashMap<String, Integer> ret = new HashMap<>();
        ArrayList<Coordinate> coList = new ArrayList<>();
        int x = co.getRow();
        int y = co.getColumn();
        for (int i = -3; i <= 3; ++i) {
            for (int j=(i<0?-i-3:i-3); j<= (i<0?i+3:3-i); ++j) {
                if (x+i<0||x+i>25||y+j<0||y+j>9) continue;
                
                coList.add(new Coordinate(x+i, y+j));
            }
        }

        for (Coordinate target: coList) {

            for (Ship<T> ship: myShips) {
                if(ship.occupiesCoordinates(target)) {
                    if (!ret.containsKey(ship.getName()))
                        ret.put(ship.getName(), 0);
                    ret.put(ship.getName(), ret.get(ship.getName())+1);
                    break;
                }
            }
        }
            
        return ret;
    }


    public BattleShipBoard(int w, int h, T miss_info) {
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }
        width = w;
        height = h;
        myShips = new ArrayList<Ship<T>>();
        placementChecker = new NoCollisionRuleChecker<T>(new InBoundsRuleChecker<T>(null));
        enemyMisses = new HashSet<>();
        enemyHits = new HashMap<>();
        missInfo = miss_info;
    }
}