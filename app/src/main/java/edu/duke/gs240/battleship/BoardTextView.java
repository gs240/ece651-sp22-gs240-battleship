package edu.duke.gs240.battleship;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the 
 * enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     * 
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }

    /**
     * This makes the body line, e.g. A 0|1|2|3|4 A\n
     *                                B 0|1|2|3|4 B
     * @return the String that is the body for the given board
     */
    String makeBody(Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder(""); // README shows two spaces at
        int width = toDisplay.getWidth();
        int height = toDisplay.getHeight();

        for (int h = 0; h < height; ++h) {
            char c = (char)('A' + h);
            ans.append(c+" ");
            for (int w = 0; w < width; w++) {
                Character ch = getSquareFn.apply(new Coordinate(h, w));
                if (ch != null) 
                    ans.append(ch);
                else 
                    ans.append(" ");
                ans.append("|");
            }
            ans.replace(ans.length()-1, ans.length(), " ");
            ans.append(c+"\n");
        }

        // ans.append("\n");
        return ans.toString();
    }

     /**
     * Constructs a BoardView, given the board it will display.
     * 
     * @param toDisplay is the Board to display
     * @throws IllegalArgumentException if the board is larger than 10x26.  
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
        throw new IllegalArgumentException(
            "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }

    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }

    public String displayEnemyBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }

    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        String ret = makeHeader() + makeBody(getSquareFn) + makeHeader();
        return ret;
    }

    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
    //     Along the same lines, the first header starts at column number 5.  We can keep
    //  that as always 5.  The second starts at 42, which we can generalize to 2*W+22.
        StringBuilder ret = new StringBuilder("     ");
        String myString = displayMyOwnBoard();
        String enemyString = enemyView.displayEnemyBoard();    
        
        ArrayList<String> myStringList = new ArrayList(Arrays.asList(myString.split("\n")));
        ArrayList<String> enemyStringList = new ArrayList(Arrays.asList(enemyString.split("\n")));

        ret.append(myHeader);
        for (int i = 0; i < 2 * toDisplay.getWidth() + 17 - myHeader.length(); ++i) 
            ret.append(" ");
        ret.append(enemyHeader+"\n");
        
        for (int i = 0; i < toDisplay.getHeight()+2; ++i) {
            // for (int j = 0; j < 5; ++j) 
            //     ret.append(" ");
            ret.append(myStringList.get(i));

            for (int j = 0; j < 2 * toDisplay.getWidth() + 19 - myStringList.get(i).length(); ++j) 
                ret.append(" ");

            // if (i == 0 || i == toDisplay.getHeight()+1) ret.append(" ");
            ret.append(enemyStringList.get(i));
            ret.append("\n");
        }
        ret.append("\n");
        return ret.toString();
    }
    
    
  }