package edu.duke.gs240.battleship;

public class Placement {
    private final char orientation;
    private final Coordinate where;

    public Placement(Coordinate co, char or) {
        where = co;
        orientation = Character.toLowerCase(or);
        if (orientation != 'v' && 
            orientation != 'h' && 
            orientation != 'u' &&
            orientation != 'r' &&
            orientation != 'd' &&
            orientation != 'l') {
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
    }

    public Placement(String all) {
        if (all.length() != 3) {
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
        where = new Coordinate(all.substring(0, 2));
        orientation = Character.toLowerCase(all.charAt(2));
        if (orientation != 'v' && 
            orientation != 'h' && 
            orientation != 'u' &&
            orientation != 'r' &&
            orientation != 'd' &&
            orientation != 'l') {
            throw new IllegalArgumentException("That placement is invalid: it does not have the correct format.");
        }
    }

    public Coordinate getCoordinate() {
        return where;
    }

    public char getOrientation() {
        return orientation;
    }

    @Override
    public String toString() {
        return orientation+where.toString();
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Placement c = (Placement) o;
            return c.orientation == orientation && c.where.equals(where);
        }
        return false;
    }
}
