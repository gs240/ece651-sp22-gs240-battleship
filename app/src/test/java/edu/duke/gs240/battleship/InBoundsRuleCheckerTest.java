package edu.duke.gs240.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
    
    @Test
    public void test_inbound_check() {
        V1ShipFactory factory = new V1ShipFactory(); 
        Placement p = new Placement(new Coordinate(3, 4), 'v');
        Ship<Character> ship1 = factory.makeDestroyer(p);
        BattleShipBoard<Character> board = new BattleShipBoard<Character>(10, 26, 'T');
        assertEquals(null, board.getChecker().checkMyRule(ship1, board));
        
    }

}
