package edu.duke.gs240.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;

import org.junit.jupiter.api.Test;

public class TextPlayerTest {
    
    private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'T');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory, true);
    }

    @Test
    void test_read_placement() throws IOException  {
        String prompt = "Please enter a location for a ship:";
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');

        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, expected[i]); //did we get the right Placement back
            assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }

    @Test
    void test_do_one_placement() throws IOException  {
        // String prompt = "Please enter a location for a ship:";
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);


        player.doOnePlacement("Submarine", (p) -> new V1ShipFactory().makeSubmarine(p));
    }

    @Test
    void test_do_placement_phase() throws IOException  {
        // String prompt = "Please enter a location for a ship:";
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        String inputString = "A0V\nA1V\nA2V\nA3V\nA4V\nA5V\nE3U\nH3U\nK3L\nO3R\nR3L\n";
        TextPlayer player = createTextPlayer(10, 26, inputString, bytes);

        player.doPlacementPhase();
    }
}
