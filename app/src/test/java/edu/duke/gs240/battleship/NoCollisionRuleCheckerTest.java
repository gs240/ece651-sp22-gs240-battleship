package edu.duke.gs240.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest{

    @Test
    public void test_no_collision() {
        V1ShipFactory factory = new V1ShipFactory(); 
        Placement p = new Placement(new Coordinate(3, 4), 'v');
        Ship<Character> ship1 = factory.makeDestroyer(p);
        BattleShipBoard<Character> board = new BattleShipBoard<Character>(10, 10, 'T');
        
        assertEquals(null, board.getChecker().checkMyRule(ship1, board));
        board.tryAddShip(ship1);

        Placement p2 = new Placement(new Coordinate(2, 4), 'v');
        Ship<Character> ship2 = factory.makeDestroyer(p2);
        assertEquals("That placement is invalid: the ship overlaps another ship.", board.getChecker().checkMyRule(ship2, board));
    }
}
