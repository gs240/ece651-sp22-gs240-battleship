package edu.duke.gs240.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {

  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'T');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'T'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'T'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'T'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'T'));
  }

  // @Test
  // public void test_hit() {
  //   BattleShipBoard<Character> b = new BattleShipBoard<Character>(3, 2);
  //   b.tryAddShip(new RectangleShip<Character>(new Coordinate(0, 2), 's', '*'));
  //   b.tryAddShip(new RectangleShip<Character>(new Coordinate(1, 2), 's', '*'));
  //   assertEquals(, actual);
  // }


  private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expect) {
    int width = b.getWidth();
    int height = b.getHeight();
    for (int h = 0; h < height; ++h) {
      for (int w = 0; w < width; ++w) {
        assertEquals(expect[h][w], b.whatIsAtForSelf(new Coordinate(h, w)));
      }
    }
  }

  @Test
  public void test_add_ship() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(3, 2, 'T');
    b.tryAddShip(new RectangleShip<Character>(new Coordinate(1, 2), 's', '*'));
    checkWhatIsAtBoard(b, 
      new Character[][] {
        {null, null, null},
        {null, null, 's'}
    });
    b.tryAddShip((new RectangleShip<Character>(new Coordinate(0, 1), 's', '*')));
    checkWhatIsAtBoard(b, 
      new Character[][] {
        {null, 's', null},
        {null, null, 's'}
    });
  }

  @Test
  public void test_try_add_ship() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(3, 2, 'T');
    b.tryAddShip(new RectangleShip<Character>(new Coordinate(1, 2), 's', '*'));
    assertEquals("That placement is invalid: the ship overlaps another ship.", b.tryAddShip(new RectangleShip<Character>(new Coordinate(1, 2), 's', '*')));
  }

  @Test
  public void test_fire_ship() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(6, 10, 'T');
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> ship = f.makeDestroyer(new Placement("A1H"));
    b.tryAddShip(ship);
    assertEquals(null, b.fireAt(new Coordinate(7, 5)));
    assertSame(ship, b.fireAt(new Coordinate(0, 1)));
    assertEquals(false, ship.isSunk());
    b.fireAt(new Coordinate(0, 2));
    b.fireAt(new Coordinate(0, 3));
    assertEquals(true, ship.isSunk());
  }

}