package edu.duke.gs240.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Executable;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
    @Test
    public void test_display_empty_2by2() {
      Board<Character> b1 = new BattleShipBoard<Character>(2, 2, 'T');
      BoardTextView view = new BoardTextView(b1);
      String expectedHeader= "  0|1\n";
      assertEquals(expectedHeader, view.makeHeader());
      String expected=
        expectedHeader+
        "A  |  A\n"+
        "B  |  B\n"+
        expectedHeader;
      assertEquals(expected, view.displayMyOwnBoard());
    }

    @Test
    public void test_display_empty_3by2() {
        String expectedHeader = "  0|1\n";
        String expectedBody = 
            "A  |  A\n"+
            "B  |  B\n"+
            "C  |  C\n";
        emptyBoardHelper(2, 3, expectedHeader, expectedBody);
    }

    @Test
    public void test_display_empty_3by5(){
        String expectedHeader = "  0|1|2|3|4\n";
        String expectedBody = 
            "A  | | | |  A\n"+
            "B  | | | |  B\n"+
            "C  | | | |  C\n";
        emptyBoardHelper(5, 3, expectedHeader, expectedBody);
    }


    @Test
    public void test_display_occupied_3by2() {
        String expectedHeader = "  0|1\n";
        String expectedBody = 
            "A s|  A\n"+
            "B  |  B\n"+
            "C  |  C\n";
        Board<Character> b1 = new BattleShipBoard<Character>(2, 3, 'T');
        b1.tryAddShip(new RectangleShip<Character>(new Coordinate(0, 0), 's', '*'));
        BoardTextView view = new BoardTextView(b1);
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
    }

    private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody){
        Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'T');
        BoardTextView view = new BoardTextView(b1);
        assertEquals(expectedHeader, view.makeHeader());
        String expected = expectedHeader + expectedBody + expectedHeader;
        assertEquals(expected, view.displayMyOwnBoard());
      }

    @Test
    public void test_invalid_board_size() {
        Board<Character> wideBoard = new BattleShipBoard<Character>(11,20, 'T');
        Board<Character> tallBoard = new BattleShipBoard<Character>(10,27, 'T');
        //you should write two assertThrows here

        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
        assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
    }

    @Test
    public void test_display_enemy() {
        // String expectedHeader = "  0|1\n";
        // String expectedBody = 
        //     "A s|  A\n"+
        //     "B  |  B\n"+
        //     "C  |  C\n";

        String myView =
      "  0|1|2|3\n" +
      "A  | | |d A\n" +
      "B s|s| |d B\n" +
      "C  | | |d C\n" +
      "  0|1|2|3\n";
        Board<Character> b1 = new BattleShipBoard<Character>(4, 3, 'T');
        V1ShipFactory f = new V1ShipFactory();
        b1.tryAddShip(f.makeDestroyer(new Placement("A3V")));
        b1.tryAddShip(f.makeSubmarine(new Placement("B0H")));
        BoardTextView view = new BoardTextView(b1);
        assertEquals(myView, view.displayMyOwnBoard());
    }

    @Test
    public void test_enemy_to_next() {
    {
      String expected = "     Your Ocean           Player B's ocean\n" +
                        "  0|1|2|3                    0|1|2|3\n" + 
                        "A s|s| |  A                A s|d|d|d A\n"+
                        "B  |d|d|d B                B s| | |  B\n "+
                        "  0|1|2|3                    0|1|2|3\n";

      Board<Character> b1 = new BattleShipBoard<Character>(4, 2, 'T');
      Board<Character> b2 = new BattleShipBoard<Character>(4, 2, 'T');
      V1ShipFactory f = new V1ShipFactory();
      b1.tryAddShip(f.makeDestroyer(new Placement("B1H")));
      b1.tryAddShip(f.makeSubmarine(new Placement("A0H")));

      b2.tryAddShip(f.makeDestroyer(new Placement("A1H")));
      b2.tryAddShip(f.makeSubmarine(new Placement("A0V")));
      BoardTextView view = new BoardTextView(b1);
      BoardTextView enemyview = new BoardTextView(b2);
      assertNotEquals(expected, view.displayMyBoardWithEnemyNextToIt(enemyview, "Your Ocean", "Player B's ocean"));
    }
  }
}
