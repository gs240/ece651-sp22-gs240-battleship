package edu.duke.gs240.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {

    @Test
    public void test_equals() {
        Coordinate c1 = new Coordinate(1, 2);
        Coordinate c3 = new Coordinate(1, 2);
        Coordinate c2 = new Coordinate(1, 3);
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c1, 'V');
        Placement p3 = new Placement(c2, 'V');
        Placement p4 = new Placement(c3, 'v');
        assertEquals(p1, p2);
        assertEquals(p1, p4);
        assertNotEquals(p2, p3);
    }

    @Test
    public void test_hashCode() {
        Coordinate c1 = new Coordinate(4, 1);
        Coordinate c2 = new Coordinate(4, 1);
        Placement p1 = new Placement(c1, 'v');
        Placement p2 = new Placement(c2, 'V');
        Placement p3 = new Placement(c1, 'h');

        assertEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p1.hashCode(), p3.hashCode());
    }

    @Test
    void test_string_constructor_valid_cases() {
        Placement p1 = new Placement("A0V");
        assertEquals(new Coordinate(0, 0), p1.getCoordinate());
        assertEquals('v', p1.getOrientation());
        Placement p2 = new Placement("D3h");
        assertEquals(new Coordinate(3, 3), p2.getCoordinate());
        assertEquals('h', p2.getOrientation());
    }
    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("0"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("00"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAAA"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("D4g"));
    }
}
