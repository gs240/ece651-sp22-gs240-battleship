package edu.duke.gs240.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RectangleShipTest {
    
    @Test
    public void test_hit() {
        RectangleShip<Character> ship = 
            new RectangleShip<Character>("name", new Coordinate(1, 2), 3, 2, 's', '*');
        ship.recordHitAt(new Coordinate(1, 2));
        ship.recordHitAt(new Coordinate(1, 3));
        assertEquals(ship.isSunk(), false);
        ship.recordHitAt(new Coordinate(1, 4));
        ship.recordHitAt(new Coordinate(2, 2));
        ship.recordHitAt(new Coordinate(2, 3));


        assertEquals(ship.wasHitAt(new Coordinate(2, 4)), false);
        ship.recordHitAt(new Coordinate(2, 4));
        assertEquals(ship.wasHitAt(new Coordinate(2, 4)), true);
        assertEquals(ship.getDisplayInfoAt(new Coordinate(2, 4), true), '*');
        assertEquals(ship.isSunk(), true);
        
    }
}
